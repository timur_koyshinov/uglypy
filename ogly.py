from patterns import pat_def, pat_if, pat_elif, pat_else, pat_for, pat_while, empt_line
from implicit_predicates import wittingly_the_same, wittingly_false
from random import choice, randint

import syntax_analysis
import json
import re


def looklike(code):
    for _ in range(8):
        k = [randint(1,2) for _ in range(7)]
        code = code.replace("False", "True") if k[0]==1 else code.replace("True", "False")
        code = code.replace("+=", "-=") if k[1]==1 else code.replace("-=", "+=")
        code = code.replace(" in ", " not in ") if k[2]==1 else code.replace(" not in ", " in ")
        code = code.replace(" or ", " and ") if k[4]==1 else code.replace(" and ", " or ")
##        code = code.replace("0", "1") if k[6]==1 else code.replace("1", "0")
    return code


def replacereturn(code):
    new_code = []
    code = code.split("\n")
    for i in code:
        s = re.search(r"return\s+([^\n\;]+)", i)
        if s:
            new_code.append(i.replace(s.group(1), "{} if {} else {}".format(
                choice(wittingly_the_same) % s.group(1),
                "(" + choice(wittingly_false).format("bool(%s)" % s.group(1)) + ")",
                s.group(1))))
        else:
            s = re.search(r"return\(([^\n\;]+)\)", i)
            if s:
                new_code.append(i.replace(s.group(1), "{} if {} else {}".format(
                    choice(wittingly_the_same) % s.group(1),
                    choice(wittingly_false).format("bool(%s)" % s.group(1)),
                    s.group(1))))
            else:
                new_code.append(i)
    print("\n".join(new_code))    
    return "\n".join(new_code)

        
def tree_to_code(data):
    code = ""
    if not data:
        return ""
    for i in data:
        block_type = i.get("block_type")
        if block_type == None:
            code += replacereturn(i.get("code")) + "\n"
        elif block_type == "def":
            code += pat_def % (
                i.get("decorators") or "",
                i.get("name"),
                i.get("input") or "",
                add_spaces(tree_to_code(i.get("code"))))
        elif block_type == "if":
            code += pat_if % (
                choice(wittingly_the_same) % i.get("if_obj").get("condition"),
                add_spaces(tree_to_code(i.get("if_obj").get("code"))))
            for j in i.get("elif_objs"):
                code += pat_elif % (
                    choice(wittingly_the_same) % j.get("condition"),
                    add_spaces(tree_to_code(j.get("code"))))
            code += pat_elif % (
                re.sub(r'\.\w+\(\w*\)', '', choice(wittingly_false).format(i.get("if_obj").get("condition"))),
                add_spaces(looklike(tree_to_code(i.get("if_obj").get("code"))))
                )
            if i.get("else_code"):
                code += pat_else % add_spaces(tree_to_code(i.get("else_code")))
        elif block_type == "for":
            code += pat_for % (
                i.get("variable"),
                i.get("sequence"),
                add_spaces(tree_to_code(i.get("code_base"))))
            if i.get("code_else"):
                code += pat_else % add_spaces(tree_to_code(i.get("code_else")))
        elif block_type == "while":
            code += pat_while % (
                i.get("condition"),
                add_spaces(tree_to_code(i.get("code_base"))))
            if i.get("code_else"):
                code += pat_else % add_spaces(tree_to_code(i.get("code_else")))
        else:
            raise Exception("Some problem")

    return code
                
        
def add_spaces(code):
    if not code:
        return ""
    return "\n".join(" " * 4 + x for x in code.split("\n"))


def remove_empty_lines(code):
    return "\n".join(x for x in code.split("\n") if not empt_line.match(x))


def obfuscate(filename):
    with open(filename) as file:
        code = file.read()

    new_filename = filename.split(".")[0]+".obf.py"

    tree = json.dumps(syntax_analysis.split_blocks(code))
    new_code = remove_empty_lines(tree_to_code(json.loads(tree)))
##    tree = json.dumps(syntax_analysis.split_blocks(new_code)) # double
##    new_code = remove_empty_lines(tree_to_code(json.loads(tree)))

    with open(new_filename, "x") as file:
        file.write(new_code)
##    print(new_code)

obfuscate("testcode/brackets.py")
obfuscate("testcode/feedpigeons.py")
obfuscate("testcode/stripedwords.py")
