import re


VOWELS = "AEIOUY"
CONSONANTS = "BCDFGHJKLMNPQRSTVWXZ"


def stripe(word):
    if len(word) < 2:
        return 0
    word = word.upper()
    s = ""
    for i in word:
        if i in VOWELS:
            s += "1"
        elif i in CONSONANTS:
            s += "0"
        else:
            return 0
    if re.search(r"^1?(?:01)*0?$", s):
        return 1
    else:
        return 0


def checkio(text):    
    return sum([stripe(x) for x in re.split(r"\W", text) if x])


#These "asserts" using only for self-checking and not necessary for auto-testing
if __name__ == '__main__':
    assert checkio("My name is ...") == 3, "All words are striped"
    assert checkio("Hello world") == 0, "No one"
    assert checkio("A quantity of striped words.") == 1, "Only of"
    assert checkio("Dog,cat,mouse,bird.Human.") == 3, "Dog, cat and human"
