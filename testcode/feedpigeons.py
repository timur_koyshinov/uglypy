def checkio(number):
    if number < 1:
        return 0
    k = 0
    r = 0
    while True:
        number -= k
        r += 1
        if number < 0:
            return(k)
        elif r > number:
            k += number
            return(k)
        else:
            k += r
            number -= r
        
    
if __name__ == '__main__':
    #These "asserts" using only for self-checking and not necessary for auto-testing
    assert checkio(1) == 1, "1st example"
    assert checkio(2) == 1, "2nd example"
    assert checkio(5) == 3, "3rd example"
    assert checkio(10) == 6, "4th example"
