def checkio(expression):
    status, brack_end = [], {"}":"{",")":"(","]":"["}
    for i in expression:
        if i in brack_end.values(): 
            status.append(i)
        elif i in brack_end.keys():
            if not status: 
                return False
            if status.pop() != brack_end[i]: 
                return False
    return not status


assert checkio("((5+3)*2+1)") == True
assert checkio("{[(3+1)+2]+}") == True
assert checkio("(3+{1-1)}") == False
assert checkio("[1+1]+(2*2)-{3/3}") == True
assert checkio("(({[(((1)-2)+3)-3]/3}-3)") == False
assert checkio("2+3") == True