from patterns import re_func, re_if_elif_else, re_elif, re_for, re_while, re_block


N = 4 # Use 4 spaces in test code


def get_function(code):
    obj = re_func.search(code)
    characts = {
        "decorators": obj.group("decorators"),
        "name": obj.group("name"),
        "input": obj.group("input"),
        "code": split_blocks(remove_spaces(obj.group("code")))
        }
    return characts


def get_if_elif_else(code):
    obj = re_if_elif_else.search(code)
    try:
        elif_obj = obj.group("elif_obj")
    except:
        elif_obj = ""
    try:
        else_obj = obj.group("code_else")
    except:
        else_obj = None

    characts = {
        "if_obj": {
            "condition": obj.group("condition_if"),
            "code": split_blocks(remove_spaces(obj.group("code_if")))
            },
        "elif_objs": elif_obj and [{
            "condition": x1,
            "code": split_blocks(remove_spaces(x2))
            } for x1, x2 in re_elif.findall(elif_obj)],
        "else_code": else_obj and split_blocks(remove_spaces(obj.group("code_else"))) if obj.group("code_else") else None 
        }
    return characts


def get_for(code):
    obj = re_for.search(code)
    characts = {
        "variable": obj.group("variable"),
        "sequence": obj.group("sequence"),
        "code_base": split_blocks(remove_spaces(obj.group("code_base"))),
        "code_else": split_blocks(remove_spaces(obj.group("code_else"))) if obj.group("code_else") else None,
        }
    return characts


def get_while(code):
    obj = re_while.search(code)
    characts = {
        "condition": obj.group("condition"),
        "code_base": split_blocks(remove_spaces(obj.group("code_base"))),
        "code_else": split_blocks(remove_spaces(obj.group("code_else"))) if obj.group("code_else") else None
        }
    return characts        


def remove_spaces(code):
    return "\n".join([i.replace("    ", "", 1) for i in code.split("\n")
                      if i.replace("    ", "", 1)])


def split_blocks(code):
    blocks = re_block.findall(code)
    tree = []
    for block, b_type in blocks:
        ch_block = {"block_type": b_type or None}
        if ch_block["block_type"]:
            ch_block.update(block_func.get(ch_block["block_type"])(block))
        else:
            ch_block["code"] = block
        tree.append(ch_block)
    return tree


block_func = {
    "def": get_function,
    "if": get_if_elif_else,
    "for": get_for,
    "while": get_while,
    }
