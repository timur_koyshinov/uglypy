import re


re_func = re.compile(
    r"(?P<decorators>(?:@[^\n]+\n)*)"
    r"def\s+(?P<name>[a-zA-Z_][a-zA-Z_0-9]*)\s*\((?P<input>[^\n]*)\): *"
    r"\n?(?P<code>"
    r"(?:(?:    )[^\n]+\n)+"
    r")",
    re.S
    )

re_if_elif_else = re.compile(
    r"if[ \t\r\f\v]+(?P<condition_if>[^\n]+): *"
    r"\n?(?P<code_if>(?:(?:    )[^\n]+\n?)+)"
    r"(?P<elif_obj>(?:elif[ \t\r\f\v]+[^\n]+: *"
    r"\n?(?:(?:    )[^\n]+\n?)+)*)"
    r"(?:else[ \t\r\f\v]*: *"
    r"\n?(?P<code_else>(?:(?:    )[^\n]+\n?)+))?",
    re.S
    )

re_elif = re.compile(
    r"elif[ \t\r\f\v]+([^\n]+):"
    r"(\n?(?:(?:    )[^\n]+\n?)+)",
    re.S)

re_for = re.compile(
    r"for (?P<variable>[^\n]+) in (?P<sequence>[^\n]+): *"
    r"\n?(?P<code_base>(?:(?:    )[^\n]+\n?)+)"
    r"(?:else[ \t\r\f\v]*:"
    r"\n?(?P<code_else>(?:(?:    )[^\n]+\n?)+))?",
    re.S
    )


re_while = re.compile(
    r"while[ \t\r\f\v]+(?P<condition>[^\n]+): *"
    r"\n?(?P<code_base>(?:(?:    )[^\n]+\n?)+)"
    r"(?:else[ \t\r\f\v]*:"
    r"\n?(?P<code_else>(?:(?:    )[^\n]+\n?)+))?",
    re.S
    )

re_block = re.compile(
    r"((?:"
    r"(?:((?:def)|(?:if)|(?:for)|(?:while)))[^\n]+\n"
    r"(?:(?:    )+[^\n]*\n?)+"
    r"(?:elif[ \t\r\f\v]+[^\n]+:"
    r"\n?(?:(?:    )[^\n]+\n?)+)*"
    r"(?:else[ \t\r\f\v]*: *"
    r"\n?(?:(?:    )[^\n]+\n?)+)?"
    r")|(?:(?:\S[^\n]*\n?(?!"
    r"(?:(?:def)|(?:if)|(?:for)|(?:while))"
    r"))+))",
    re.S
    )


pat_def = """%s
def %s(%s):
%s
"""

pat_if = """if %s:
%s
"""

pat_elif = """elif %s:
%s
"""

pat_else = """else:
%s
"""

pat_for = """for %s in %s:
%s
"""

pat_while = """while %s:
%s
"""

empt_line = re.compile(r"\s*$")
