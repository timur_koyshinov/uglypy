from random import choice, randint


##wittingly_true = [
##    "(({}) * 0x80 & 0x56) == 0",
##    "(int({}) %% 0x38 + 0xe4df62c8 & 0x6d755e00) == 0x6455420",
##    "(({}) << 1 ^ 0x1765) != 0",
##    ]

wittingly_false = [
    "(0x29384 % (int({})+0x6)*3) >= 20",
    "set('abcdefgh') > {{{}}} or 0x2342 >> 0x35",
    "131 <= sum(map(ord, str({}))) <= 231",
    ]

wittingly_true_w = [
    "(({}) * 0x80 & 0x56) == 0".format(randint(0, 9999)),
    "(int({}) %% 0x38 + 0xe4df62c8 & 0x6d755e00) > 0x6455420".format(choice([True, False])),
    "(({}) << 1 ^ 0x1765) != 0".format(randint(0, 9999)),
    ]

wittingly_false_w = [
    "(0x29384 %% (int({})+0x6)*3) >= 20".format(choice([True, False])),
    "set('abcdefgh') > {{{}}} or 0x2342 >> 0x35".format(choice([True, False])),
    "131 <= sum(map(ord, str({}))) <= 231".format(choice([True, False])),
    ]

wittingly_the_same = [
    "(%s) and not ({})".format(choice(wittingly_false_w)),
    "(%s) and ({})".format(choice(wittingly_true_w)),
    "({}) or (%s)".format(choice(wittingly_false_w)),
    "not ({}) or (%s)".format(choice(wittingly_true_w)),
    ]
